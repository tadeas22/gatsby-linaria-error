import React from "react";
import { styled } from "linaria/react";

function CoolComponent({ className, style }) {
	return (
		<div className={className} style={style}>
			...
		</div>
	);
}

export default styled(CoolComponent)`
	width: 138px;
	margin-right: 24px;
`;
